import html
import json
import logging
import time
import traceback
from typing import Dict

from telegram import Update, ParseMode
from telegram.ext import Updater, CommandHandler, CallbackContext, Defaults, Filters, MessageHandler

from server import Server
from task import CompletedTask

class Bot:
    """
    Телеграм бот, занимается обработкой запросов от пользователей (по сути - клиент)

    Bot запускает Server, занимается обработкой запросов и отображением результатов,
    также устаналивает ограничение на частоту использование сервиса.
    """

    SEARCH_INTERVAL = 20

    def __init__(self, token: str):
        # Создается инстанс бота, запускает Server
        self.server: Server = Server()
        self.last_use: Dict[int, float] = {}

        defaults = Defaults(parse_mode=ParseMode.HTML)
        self.updater = Updater(token, use_context=True, defaults=defaults)
        dp = self.updater.dispatcher

        dp.add_error_handler(self._error_handler)

        # -*- Admin handlers -*-
        # dp.add_handler(CommandHandler('info', AdminHandler.info))
        # dp.add_handler(CommandHandler('fr', AdminHandler.full_report, pass_args=True))

        # -*- User handlers -*-
        dp.add_handler(CommandHandler('start', self._start_handler))
        dp.add_handler(MessageHandler(Filters.text, self._message_handler))

        self.bot = self.updater.bot
        self.updater.start_polling()

    def idle(self):
        self.updater.idle()
        logging.info("Scheduler stopped")

    def _start_handler(self, update: Update, context: CallbackContext):
        # Обработка сообщения /start
        chat_id = update.effective_user.id
        context.bot.send_message(
            chat_id=chat_id,
            text="Hello",
        )

    def _message_handler(self, update: Update, context: CallbackContext):
        # Обработка сообщений отличных от /start
        chat_id = update.effective_user.id
        message_text = update.message.text

        last_use = 0 if self.last_use.get(chat_id) is None else self.last_use.get(chat_id)
        if time.time() - last_use < self.SEARCH_INTERVAL:
            context.bot.send_message(
                chat_id=chat_id,
                text=f"Слишком часто, между запросами должно пройти не менее {self.SEARCH_INTERVAL} секунд"
            )
            return

        self.server.submit_task(message_text, self._result_handler, chat_id=chat_id)
        self.last_use[chat_id] = time.time()

        context.bot.send_message(
            chat_id=chat_id,
            text=f"Взяли в обработку (процесс может занимать до {self.SEARCH_INTERVAL} секунд)"
        )

    def _result_handler(self, task: CompletedTask):
        # Отображение результатов поиска
        text = '\n'.join(map(lambda x: f"{x[0]}: {x[1]}", task.fields.items()))
        text += '\n\nДля дальнейшего поиска введи имя и фамилию в формате Иван Иванов'

        self.bot.send_message(
            chat_id=task.data['chat_id'],
            text=text,
        )

    def _error_handler(self, update, context):
        # Обработчик ошибок
        logging.getLogger().info(msg="Исключение при обработке:", exc_info=context.error)
        tb_list = traceback.format_exception(None, context.error, context.error.__traceback__)
        tb_string = ''.join(tb_list)

        update_str = update.to_dict() if isinstance(update, Update) else str(update)
        message = (
            f'Возникло исключение при обработке.\n'
            f'<pre>update = {html.escape(json.dumps(update_str, indent=2, ensure_ascii=False))}'
            '</pre>\n\n'
            f'<pre>context.chat_data = {html.escape(str(context.chat_data))}</pre>\n\n'
            f'<pre>context.user_data = {html.escape(str(context.user_data))}</pre>\n\n'
            f'<pre>{html.escape(tb_string)}</pre>'
        )
