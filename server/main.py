import logging
import os
import sys

from bot import Bot


logging.basicConfig(format='[%(asctime)s] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.INFO,
                        filename=f"log.txt")
log = logging.getLogger()
log.level = logging.INFO
log.addHandler(logging.StreamHandler(sys.stderr))



def main():
    # Точка входа
    token = os.getenv("TELEGRAM_BOT_TOKEN")
    bot = Bot(token)
    bot.idle()


if __name__ == '__main__':
    main()
