import os
from threading import Thread
from typing import List
from xmlrpc.server import SimpleXMLRPCServer

from task import Task, CompletedTask


class Server:
    """
    Сервер получает задания на выполнение и кладет их в общую очередь
    При получении запроса от воркера, отдает ему задание из очереди
    """
    def __init__(self):
        host = os.getenv("RPC_SERVER_HOST")
        port = int(os.getenv("RPC_SERVER_PORT"))

        self.queue: List[Task] = list()
        self.callbacks = dict()

        self.host = host
        self.port = port
        self.rpc_server = SimpleXMLRPCServer((host, port))

        self.rpc_server.register_introspection_functions()
        self.rpc_server.register_function(self.get_task, "get_task")
        self.rpc_server.register_function(self.send_task, "send_task")

        self.thread = Thread(target=self.run)
        self.thread.start()


    def run(self):
        print(f"RPC server listening on port {self.port}...")
        self.rpc_server.serve_forever()

    def get_task(self) -> dict:
        # метод для вызова по RPC
        # XMLRPC поддерживаем передачу только примитивов, поэтому данные сериализуются/десериализутся в dict
        if len(self.queue) > 0:
            task = self.queue.pop(0)
            return task.to_dict()
        else:
            return {}

    def send_task(self, task_data: dict):
        # метод для вызова по RPC
        # XMLRPC поддерживаем передачу только примитивов, поэтому данные сериализуются/десериализутся в dict
        print(task_data)
        completed_task = CompletedTask(task_data)
        self.callbacks[completed_task.task_id](completed_task)
        return True

    def submit_task(self, name: str, callback, **kwargs):
        # клиентский метод для создания новой задачи на выполнение
        new_task = Task(name, **kwargs)
        self.callbacks[new_task.task_id] = callback

        self.queue.append(new_task)
