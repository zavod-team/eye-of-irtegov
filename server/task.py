class Task:
    """
    Entity для сериализации/десериализации входных данных
    """
    NEXT_JOB_ID = 1

    def __init__(self, name: str, **kwargs):
        self.task_id = Task.NEXT_JOB_ID
        self.name = name
        self.kwargs = kwargs

        Task.NEXT_JOB_ID += 1


    def to_dict(self):
        return {
            "task_id": self.task_id,
            "fields": {
                "name": self.name,
            },
        } | self.kwargs


class CompletedTask:
    """
    Entity для сериализации/десериализации выходных данных
    """

    def __init__(self, data: dict):
        self.task_id = data['task_id']
        self.fields = data['fields']
        self.data = data