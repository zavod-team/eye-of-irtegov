import logging
import sys

from worker import Worker

logging.basicConfig(format='[%(asctime)s] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.INFO,
                        filename=f"log.txt")
log = logging.getLogger()
log.level = logging.INFO
log.addHandler(logging.StreamHandler(sys.stderr))

def main():
    # Точка входа
    worker = Worker()
    worker.start()


if __name__ == '__main__':
    main()