from task import Fields


class BaseProcessor:
    """
    Интерфейс процессора (обработчика ресурса).

    Для создания нового процессора необходимо создать новый класс, унаследоваться от этого базового класса,
    реализовать метод perform() и указать REQUIRED_FIELDS.
    После чего добавить этот класс в список процессоров в классе Worker
    """
    REQUIRED_FIELDS = (Fields.NAME, )

    @classmethod
    def perform(cls, task):
        raise NotImplementedError("perform() method must be implemented")
