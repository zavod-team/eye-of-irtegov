import requests

from processors.base_processor import BaseProcessor
from task import Task, Fields


class GithubProcessor(BaseProcessor):
    REQUIRED_FIELDS = (Fields.USERNAME, )

    print("Github processor inited successful")

    @classmethod
    def perform(cls, task: Task):
        res = requests.get(f'http://github.com/{task.get_field(Fields.USERNAME)}')
        if res.status_code == 200:
            task.set_field(Fields.GITHUB_URL, f'https://github.com/{task.get_field(Fields.USERNAME)}')
