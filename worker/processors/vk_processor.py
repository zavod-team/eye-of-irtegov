import os

import vk_api

from processors.base_processor import BaseProcessor
from task import Task, Fields


class VkProcessor(BaseProcessor):
    REQUIRED_FIELDS = (Fields.NAME, )


    USERNAME = os.getenv("VK_API_USERNAME")
    PASSWORD = os.getenv("VK_API_PASSWORD")

    VK_SESSION = vk_api.VkApi(USERNAME, PASSWORD)
    VK_SESSION.auth()
    VK = VK_SESSION.get_api()
    print("VK processor inited successful")

    @classmethod
    def perform(cls, task: Task):
        fields = "activities,about,blacklisted,blacklisted_by_me,books,bdate,can_be_invited_group,can_post,can_see_all_posts,can_see_audio,can_send_friend_request,can_write_private_message,career,common_count,connections,contacts,city,country,crop_photo,domain,education,exports,followers_count,friend_status,has_photo,has_mobile,home_town,sex,site,schools,screen_name,status,verified,games,interests,is_favorite,is_friend,is_hidden_from_feed,last_seen,maiden_name,nickname,occupation,online,personal,photo_id,tv,universities"
        res = cls.VK.users.search(q=task.get_field(Fields.NAME), fields=fields)

        if res['count'] > 0:
            profile = res['items'][0]
            task.set_field(Fields.VK_URL, f"vk.com/id{profile['id']}")

            username = profile.get('domain')
            if username not in ("", None):
                task.set_field(Fields.USERNAME, username)

            birthdate = profile.get('bdate')
            if birthdate not in ("", None):
                task.set_field(Fields.BIRTHDATE, birthdate)

        print(res)
