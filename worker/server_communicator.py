import os
import xmlrpc.client

from task import Task


class ServerCommunicator:
    """
    Отвечает за коммуникацию с RPC сервером.

    Любые запросы к серверу должны происходить через этот класс (абстракция для остальных слоев)
    """
    def __init__(self):
        self.host = os.getenv("RPC_SERVER_HOST")
        self.port = os.getenv("RPC_SERVER_PORT")

        self.server = xmlrpc.client.ServerProxy(f"http://{self.host}:{self.port}/")

    def get_task(self) -> Task | None:
        try:
            task = self.server.get_task()
        except ConnectionRefusedError:
            print("Server conncetion refused")
            return None

        print(task)

        if len(task) > 0:
            return Task(task)
        else:
            return None

    def send_task(self, task: Task):
        self.server.send_task(task.to_dict())