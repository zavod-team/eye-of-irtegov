class Fields:
    NAME = "name"
    USERNAME = "username"
    BIRTHDATE = "birthdate"

    VK_URL = "vk_url"
    GITHUB_URL = "github_url"


class Task:
    # Entity для сериализации/десериализации данных

    def __init__(self, data: dict):
        self.data = data
        self.fields = data.get("fields")

    def get_field(self, key):
        return self.fields.get(key)

    def set_field(self, key, value):
        self.fields[key] = value

    def to_dict(self):
        self.data["fields"] = self.fields
        return self.data

    def has_field(self, key) -> bool:
        field = self.fields.get(key)
        if field in ("", None):
            return False
        else:
            return True
