import logging
import time
from typing import List, Type

from processors.base_processor import BaseProcessor
from processors.github_processor import GithubProcessor
from processors.vk_processor import VkProcessor
from server_communicator import ServerCommunicator
from task import Task


class Worker:
    """
    Класс главного процесса воркера.

    Опрашивает сервер на наличие новых заданий, занимается запуском процессоров
    """
    LOG = logging.getLogger()

    def __init__(self):
        self.server_communicator = ServerCommunicator()
        self.processors: List[Type[BaseProcessor]] = [GithubProcessor, VkProcessor]

    def start(self):
        last_update_time = 0

        while True:
            if time.time() - last_update_time > 1:
                last_update_time = time.time()
                task = self.server_communicator.get_task()
                if task is not None:
                    self.process(task)

            time.sleep(0.01)

    def process(self, task: Task):
        # Запускает обработку задания
        completed_processors = []

        # Так как существует агрегация данных, недостаточно вызвать каждый процессор поочереди
        # Может случится так, что необходимые данные для запуска первого процессора появятся только в конце
        # для решения проблемы запускаем все процессоры подряд N раз, при этом каждый запускаем не более одного раза
        for _ in range(len(self.processors)):
            for processor in self.processors:
                if processor not in completed_processors and self.is_runnable(processor, task):
                    try:
                        processor.perform(task)
                    except Exception as exc:
                        self.LOG.error(exc)
                    finally:
                        completed_processors.append(processor)

        self.server_communicator.send_task(task)

    def is_runnable(self, processor: Type[BaseProcessor], task: Task) -> bool:
        # проверяет, возможно ли запустить ли процессор с текущими полями в Task
        res = True

        for field in processor.REQUIRED_FIELDS:
            if not task.has_field(field):
                res = False

        return res
